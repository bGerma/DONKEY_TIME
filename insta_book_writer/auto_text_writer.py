"""
musica
"""
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver import Chrome


def insta_login(driver, email, pwd) -> None:
    """logs into discord"""
    driver.get('https://www.instagram.com')
    time.sleep(4)
    driver.find_element(By.XPATH, '/html/body/div[4]/div/div/button[1]').click()
    email_box = driver.find_element(By.NAME, 'username')
    email_box.send_keys(email)
    pwd_box = driver.find_element(By.NAME, 'password')
    pwd_box.send_keys(pwd)
    pwd_box.send_keys(Keys.RETURN)


def go_to_gc(driver) -> None:
    """finds and selects the donkey time thing"""

    driver.find_element(By.XPATH, '/html/body/div[1]/section/main/div/div/div/div/button').click()
    driver.find_element(By.XPATH, '/html/body/div[5]/div/div/div/div[3]/button[2]').click()
    driver.find_element(By.XPATH, '/html/body/div[1]/section/nav/div[2]/div/div/div[3]/div/div[2]/a').click()
    time.sleep(3)
    driver.find_element(By.XPATH, '/html/body/div[1]/section/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[2]/a/div').click()


def send_text(driver) -> None:
    """enters the donkey times"""
    text_bar = driver.find_element(By.XPATH, '/html/body/div[1]/section/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[2]/textarea')
    with open("./text_to_send.txt") as text:
        for line in text:
            text_bar.send_keys(line)
            time.sleep(1)


def auto_text_writer() -> None:
    """the entire program"""
    username, password, = 'limpbizkit_bumper', 'epicgaymer69'
    driver = Chrome(executable_path='C:/Users/Brooklyn/Downloads/Tools/chromedriver.exe')
    insta_login(driver, username, password)
    time.sleep(6)
    go_to_gc(driver)
    time.sleep(4)
    send_text(driver)
    time.sleep(10000)


if __name__ == '__main__':
    auto_text_writer()