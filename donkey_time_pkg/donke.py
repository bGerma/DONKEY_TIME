"""
donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke donke
"""
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver import Chrome


def discord_login(driver, email=None, pwd=None) -> None:
    """logs into discord"""
    driver.get('https://www.discord.com/login')
    time.sleep(4)
    email_box = driver.find_element(By.NAME, 'email')
    email_box.send_keys(email)
    pwd_box = driver.find_element(By.NAME, 'password')
    pwd_box.send_keys(pwd)
    pwd_box.send_keys(Keys.RETURN)


def go_to_donkey_time(driver) -> None:
    """finds and selects the donkey time thing"""
    search_bar_select = driver.find_element(By.CLASS_NAME, 'searchBarComponent-32dTOx')
    search_bar_select.send_keys(Keys.RETURN)
    time.sleep(5)
    search_bar = driver.find_element(By.CLASS_NAME, 'input-2VB9rf')
    search_bar.send_keys('#☄spam-babies')
    search_bar.send_keys(Keys.RETURN)


def enter_donky_time(driver, num_of_donkey_times: int) -> None:
    """enters the donkey times"""
    donkey_enter_bar = driver.find_element(By.XPATH, '/html/body/div/div[2]/div/div[2]/div/div/div/div[2]/div[2]/div[2]'
                                                     '/main/form/div/div/div/div/div/div[3]/div[2]')
    for _ in range(num_of_donkey_times):
        donkey_enter_bar.send_keys('https://media.discordapp.net/attachments/415530594743746561/'
                                   '835215011147546664/image0.gif')
        donkey_enter_bar.send_keys(Keys.RETURN)
        time.sleep(3)


def donkey_time() -> None:
    """the entire program lol"""
    # email, pwd, num_of_donkeys = input('email: '), input('password: '), input('how many donkeys: ')
    email, pwd, num_of_imposter = 'brooklyn.germa@gmail.com', 'dabonthehaters', input('imposters😳: ')
    web_driver = Chrome(executable_path='C:/Users/Brooklyn/Downloads/Tools/chromedriver.exe')
    discord_login(web_driver, email, pwd)
    time.sleep(6)
    go_to_donkey_time(web_driver)
    time.sleep(4)
    enter_donky_time(web_driver, int(num_of_imposter))
    time.sleep(10000)
    pass


if __name__ == '__main__':
    donkey_time()
